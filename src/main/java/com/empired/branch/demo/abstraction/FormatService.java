package com.empired.branch.demo.abstraction;

public interface FormatService {

	public String formatName(String name);

}
