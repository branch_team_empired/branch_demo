package com.empired.branch.demo.abstraction;


import com.empired.branch.demo.DemoFeatures;
import com.empired.branch.demo.impl.ExistingImpl;
import com.empired.branch.demo.impl.NewImpl;

public class NameAbstraction implements FormatService {

	FormatService service;

	public NameAbstraction() {
		
		/*
		String env = System.getProperty("environment");
		
		if("dev".equals(env)){
			service = new NewImpl();
		} else {
			service = new ExistingImpl();
		}
		*/
		
		if(DemoFeatures.NEW_SERVICE_IMPL.isActive()){
			service = new NewImpl();
		} else {
			service = new ExistingImpl();
		}
	}

	@Override
	public String formatName(String name) {
		return service.formatName(name);
	}
}
