package com.empired.branch.demo;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.empired.branch.demo.abstraction.NameAbstraction;

@ManagedBean(name = "helloWorld")
@RequestScoped
public class HelloWorld {

    private String name;
	
	private NameAbstraction abstraction;

    public HelloWorld() {
    	abstraction = new NameAbstraction();
    }

    public String send() {
        return "worldAnswer.xhtml";
    }

    public String getName() {
        return abstraction.formatName(name);
    }

    public void setName(String name) {
        this.name = name;
    }
}
