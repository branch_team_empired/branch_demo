package com.empired.branch.demo.impl;

import com.empired.branch.demo.abstraction.FormatService;

public class NewImpl implements FormatService {

	public String formatName(String name) {
		if (name != null) {
			return String.format("'%s - (New Impl)'", name);
		} else {
			return null;
		}
	}
}
